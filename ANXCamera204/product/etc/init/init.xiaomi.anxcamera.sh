#!/system/bin/sh

sleep 5

#rm -rf /storage/emulated/0/.ANXCamera/cheatcodes/
#mkdir -p /storage/emulated/0/.ANXCamera/cheatcodes/
#cp -R /system/etc/ANXCamera/cheatcodes/* /storage/emulated/0/.ANXCamera/cheatcodes/


#rm -rf /storage/emulated/0/.ANXCamera/cheatcodes_reference/
#mkdir -p /sdstorage/emulated/0card/.ANXCamera/cheatcodes_reference/
#cp -R /system/etc/ANXCamera/cheatcodes/* /storage/emulated/0/.ANXCamera/cheatcodes_reference/


#rm -rf /storage/emulated/0/.ANXCamera/features/
#mkdir -p /storage/emulated/0/.ANXCamera/features/
#cp -R /system/etc/device_features/* /storage/emulated/0/.ANXCamera/features/

#rm -rf /storage/emulated/0/.ANXCamera/features_reference/
#mkdir -p /storage/emulated/0/.ANXCamera/features_reference/
#cp -R /system/etc/device_features/* /storage/emulated/0/.ANXCamera/features_reference/

rm -f /data/vendor/camera/*.nv12

pm grant com.android.camera android.permission.CAMERA
pm grant com.android.camera android.permission.ACCESS_FINE_LOCATION
pm grant com.android.camera android.permission.ACCESS_COARSE_LOCATION
pm grant com.android.camera android.permission.RECORD_AUDIO
pm grant com.android.camera android.permission.READ_PHONE_STATE
pm grant com.android.camera android.permission.READ_EXTERNAL_STORAGE
pm grant com.android.camera android.permission.WRITE_EXTERNAL_STORAGE
pm grant com.android.camera android.permission.MANAGE_EXTERNAL_STORAGE
pm grant com.android.camera android.permission.ACCESS_MEDIA_LOCATION
pm grant com.android.camera android.permission.WRITE_MEDIA_STORAGE
pm grant com.android.camera android.permission.INTERACT_ACROSS_USERS
pm grant com.android.camera android.permission.WRITE_SETTINGS
pm grant com.android.camera android.permission.WRITE_SECURE_SETTINGS
pm grant com.android.camera android.permission.PACKAGE_USAGE_STATS
pm grant com.android.camera android.permission.WAKE_LOCK
pm grant com.android.camera android.permission.MODIFY_AUDIO_SETTINGS
pm grant com.android.camera android.permission.FOREGROUND_SERVICE
pm grant com.android.camera android.permission.ACCESS_NETWORK_STATE
pm grant com.android.camera android.permission.ACCESS_WIFI_STATE
pm grant com.android.camera android.permission.CHANGE_WIFI_STATE
pm grant com.android.camera android.permission.INTERNET
pm grant com.android.camera android.permission.BIND_JOB_SERVICE
pm grant com.android.camera android.permission.INSTALL_PACKAGES
pm grant com.android.camera android.permission.VIBRATE
pm grant com.android.camera android.permission.DEVICE_POWER
pm grant com.android.camera android.permission.RECEIVE_BOOT_COMPLETED
pm grant com.android.camera android.permission.CONTROL_DISPLAY_BRIGHTNESS
pm grant com.android.camera com.android.camera.permission.RESET_PREF
pm grant com.android.camera com.android.camera.permission.SPLASH
pm grant com.android.camera com.android.settings.permission.CLOUD_SETTINGS_PROVIDER
pm grant com.android.camera com.fingerprints.service.ACCESS_FINGERPRINT_MANAGER
pm grant com.android.camera com.android.SystemUI.permission.TIGGER_TOGGLE
pm grant com.android.camera com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE
pm grant com.android.camera com.google.android.c2dm.permission.RECEIVE
pm grant com.android.camera com.google.android.c2dm.permission.SEND

pm grant com.android.camera com.miui.gallery.permission.BIND_SERVICE
pm grant com.android.camera mediatek.permission.ACCESS_APU_SYS
pm grant com.android.camera com.xiaomi.scanner.receiver.RECEIVER
pm grant com.android.camera com.android.camera.permission.SPLASH
pm grant com.android.camera android.permission.START_ACTIVITIES_FROM_BACKGROUND
pm grant com.android.camera android.permission.INJECT_EVENTS

pm grant com.android.camera android.permission.INTERACT_ACROSS_USERS_FULL
pm grant com.android.camera android.permission.FOREGROUND_SERVICE
pm grant com.android.camera android.permission.INTERACT_ACROSS_USERS



appops set --uid 0 LEGACY_STORAGE allow
appops set com.android.camera READ_EXTERNAL_STORAGE allow
appops set com.android.camera WRITE_EXTERNAL_STORAGE allow
appops set com.android.camera READ_MEDIA_AUDIO allow
appops set com.android.camera READ_MEDIA_VIDEO allow
appops set com.android.camera READ_MEDIA_IMAGES allow
appops set com.android.camera WRITE_MEDIA_AUDIO allow
appops set com.android.camera WRITE_MEDIA_VIDEO allow
appops set com.android.camera WRITE_MEDIA_IMAGES allow
appops set com.android.camera MANAGE_EXTERNAL_STORAGE allow
appops set com.android.camera NO_ISOLATED_STORAGE allow


setprop persist.vendor.camera.mialgo.sddump false
setprop persist.vendor.camera.dumpPreview false
setprop persist.vendor.camera.miaihdrDisC2 false
setprop persist.vendor.camera.miaihdrDisP1 false
setprop persist.vendor.camera.checkerThre false
setprop persist.vendor.camera.enableXiaomiDebugInfo false
setprop persist.vendor.camera.hdr.checkerdump false
